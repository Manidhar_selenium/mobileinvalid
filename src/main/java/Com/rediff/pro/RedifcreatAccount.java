package Com.rediff.pro;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import Com.rediff.uills.Common;

public class RedifcreatAccount extends Common{

	@BeforeMethod
	public void openbrowser() throws IOException {
		LoadFiles();
		lodFile2();
		openBrowser();
		
		
	
	}
	@Test
public void varifyCreatAccount	() throws InterruptedException, IOException {
		Thread.sleep(2000);

		SoftAssert softassertion=new SoftAssert();
		try {
		driver.findElement(By.linkText(objects.getProperty("CreateAccountlinkText"))).click();
		driver.findElement(By.xpath(objects.getProperty("FullName"))).sendKeys("SAI NIKHIL");
		driver.findElement(By.xpath(objects.getProperty("Rediffmailid"))).sendKeys("sainikhil");
		driver.findElement(By.xpath(objects.getProperty("CheckAvalibulity"))).click();
		Thread.sleep(3000);
		String actualerrormessage=driver.findElement(By.xpath(objects.getProperty("Sorrymessage"))).getText();
		String expectederrormessage="Sorry, the ID that you are looking for is taken.";
		System.out.println("actualerrormessage");
		System.out.println("expectederrormessage");
		softassertion.assertEquals(actualerrormessage, expectederrormessage);
		
		softassertion.assertAll();
	}catch(Exception e) {
		CaptureScreenShot();
		e.printStackTrace();
		softassertion.assertAll();

		}
	
	}

	@AfterMethod
	public void closebrowser(){
		closeBrowser();
	}
	
	
}

